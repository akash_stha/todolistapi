var express = require('express');
var app = express();
var morgan = require('morgan');
var routes = require('./api/routes/todoListRoutes')

var bodyParser = require('body-parser');

require('./db')
var port = process.env.PORT || 6000;

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

routes(app);


app.listen(port, function(err, done){
    if(err){
        console.log('serve connection failed',err)
    }else{
        console.log('server connection successfull at ',port);
    }
})