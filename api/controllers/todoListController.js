var taskModel = require('./../models/todoListModel')

exports.list_all_tasks = function(req, res){
    taskModel.find({}, function(err, task){
        if(err){
            res.send(err);
        }else{
            res.json(task)
        }
    })
}

exports.create_a_task = function(req, res){
    var new_task = new taskModel(req.body);
    new_task.save(function(err,task){
        if(err){
            res.send(err)
        }else{
            res.json(task);
        }
    })
}

exports.read_a_task = function(req,res){
    taskModel.findById(req.params.taskId, function(err, task){
        if(err){
            res.send(err)
        }else{
            res.json(task)
        }
    })
}

exports.update_a_task = function(req, res){
    taskModel.findByIdAndUpdate({_id:req.params.taskId}, req.body, {new: true}, function(err, task){
        if(err){
            res.send(err)
            console.log(err);
        }else{
            res.json(task)
        }
    })
}


exports.delete_a_task = function(req, res){
    taskModel.remove({_id:req.params.taskId}, function(err, task){
        if(err){
            res.send(err)
        }else{
            res.json(task)
            // res.json('task deleted')
        }
    })
}